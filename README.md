# SimpleTip

A simple tip calculator, caclulates a 15% tip.

- No ads
- No data collection
- No permissions needed
- No options

Just the tip.

![Screenshot](https://gitlab.com/erickveil1/simpletip/-/raw/master/media/playStore/SimpleTipScreenShot.jpg)

