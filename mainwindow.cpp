#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pbCalculate_clicked()
{
   double billAmt = ui->dspBillAmt->value();
   double tipAmt = billAmt * 0.15;
   double totalAmt = billAmt + tipAmt;

   char format = 'f';
   int precision = 2;
   ui->lblTipAmt->setText("$" + QString::number(tipAmt, format, precision));
   ui->lblTotalAmt->setText("$" + QString::number(totalAmt, format, precision));

}

